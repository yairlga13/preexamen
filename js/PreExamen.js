
function fetchData() {
    var userId = document.getElementById('inputID').value;
    axios.get('https://jsonplaceholder.typicode.com/users/' + userId)
        .then(function(response) {
            var user = response.data;
            document.getElementById('inputNombre').value = user.name;
            document.getElementById('inputNombreUsuario').value = user.username;
            document.getElementById('inputEmail').value = user.email;
            document.getElementById('inputDomicilioCalle').value = user.address.street;
            document.getElementById('inputDomicilioNumero').value = user.address.suite;
            document.getElementById('inputDomicilioCiudad').value = user.address.city;
        })
        .catch(function(error) {
            console.error(error);
        });
}

document.getElementById("btnBuscar").addEventListener('click',function(){
    fetchData();
});
